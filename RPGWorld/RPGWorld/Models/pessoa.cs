﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RPGWorld.Models
{
    public class pessoa
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public int druid { get; set; }
        public int sorcerer { get; set; }
        public int paladin { get; set; }
        public int knight { get; set; }
    }
}