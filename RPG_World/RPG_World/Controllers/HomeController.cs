﻿using RPG_World.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;

namespace RPG_World.Controllers
{
    public class HomeController : Controller
    {
        private Contexto db = new Contexto();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,nome,classe_a,classe_b,classe_c,classe_d")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("Details");
            }

            return View(usuario);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Descubra()
        {
            var usuario = new Usuario
            {
                
            }

            return View();
        }
        public ActionResult Resultado()
        {
            
            return View();
        }
    }
}
